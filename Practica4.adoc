===== Adria Suarez Hidalgo
//Practica 3
= Pràctica 4. Pràctica DNS
//Taula de contingut
:doctype: article
:encoding: utf-8
:lang: ca
:toc: left
:toclevels: 3
:numbered:

= Apartat 1: Instal·lació del servidor DNS a Zoidberg

Captura del resultat de la consulta DNS.

image::imatges/p4/ex1/1.JPG[]

---

Captura de la instrucció que instal·la el servidor DNS.

image::imatges/p4/ex1/2.JPG[]

---

Captura del resultat de la instrucció netstat que mostra el servidor DNS funcionant.

image::imatges/p4/ex1/3.JPG[]

---

Captura de la execució de systemd-resolve --status

image::imatges/p4/ex1/4.JPG[]

---

Captura de la instrucció utilitzada.

image::imatges/p4/ex1/5.JPG[]

---

Captura de /etc/default/named

image::imatges/p4/ex1/6.JPG[]

---

Captura de /etc/bind/named.conf.options

image::imatges/p4/ex1/7.JPG[]

---

Captura de la instrucció utilitzada i el seu resultat.

image::imatges/p4/ex1/8.JPG[]

---

Part del registre del bind corresponent a l’última petició.

image::imatges/p4/ex1/9.JPG[]

---

Instrucció per bolcar el cau a un fitxer i contingut obtingut.

image::imatges/p4/ex1/10.JPG[]

---

Instrucció per desactivar la depuració del servidor DNS.

image::imatges/p4/ex1/11.JPG[]

---

= Apartat 2: Configuració d’un servidor DNS només cache

Captura de les modificacions fetes a la configuració.

image::imatges/p4/ex2/1.JPG[]

---

Captura de les modificacions fetes a la configuració.

image::imatges/p4/ex2/2.JPG[]

---

Sentències utilitzades i resultats

image::imatges/p4/ex2/3.JPG[]

[source,bash]
----
Comprova que no te errors.
----
---

image::imatges/p4/ex2/4.JPG[]

[source,bash]
----
Reinicia DNS i activa depuracio BIND.
----
---

image::imatges/p4/ex2/5.JPG[]

[source,bash]
----
Mirem els canvis del servidor.
----
---

image::imatges/p4/ex2/6.JPG[]

[source,bash]
----
Tanquem la depuracio BIND.
----
---









