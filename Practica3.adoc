===== Adria Suarez Hidalgo
//Practica 3
= Pràctica 3. Pràctica servidor DHCP
//Taula de contingut
:doctype: article
:encoding: utf-8
:lang: ca
:toc: left
:toclevels: 3
:numbered:

= Apartat 1: Instal·lació del servidor DHCP a Farnsworth

Captura del contingut del fitxer /etc/default/isc-dhcp-server.

image::imatges/p3/ex1/1.JPG[]

[source,bash]
----
nano /etc/default/isc-dhcp-server
----
---

Ruta completa a l’arxiu de configuració.

image::imatges/p3/ex1/2.JPG[]

[source,bash]
----
nano /etc/dhcp/dhcpd.conf

Per reinicia el servei: sudo service restart
----
---

Captura del resultat de la instrucció anterior.


image::imatges/p3/ex1/3.JPG[]

[source,bash]
----
sudo cat /var/log/syslog | grep dhcpd
----
---

Explica el significat dels missatges de la captura anterior. Què estem veient?

[source,bash]
----
Tota la configuracio que fa el servidor automaticament de dgcp.
----



= Apartat 2: Servidor DHCP a la mateixa xarxa

Arxiu de configuració complet, esborreu tots els comentaris i feu-lo llegible.

image::imatges/p3/ex1/4.JPG[]

[source,bash]
----
sudo nano /etc/netplan/01-netcfg.yaml
----
---

= Apartat 3: Configuració de la màquina client PC1 (a la mateixa xarxa)

Mostrar configuració de xarxa de PC1

image::imatges/p3/ex1/6.JPG[]

[source,bash]
----
sudo nano /etc/netplan/01-netcfg.yaml
----
---

Comanda per demanar una nova IP amb dhclient


[source,bash]
----
sudo dhclient
----
---

Captura de les últimes línies del syslog de Farnsworth.

image::imatges/p3/ex1/9.JPG[]

[source,bash]
----
sudo tail -f /var/log/syslog
----
---

Captura de pantalla amb la sortida del tcpdump.

image::imatges/p3/ex1/10.JPG[]

[source,bash]
----
sudo tcpdump -v -n
----
---

Validar l’adreça ip

image::imatges/p3/ex1/12.JPG[]

[source,bash]
----
ifconfig -a
----
---

Validar la porta d’enllaç

image::imatges/p3/ex1/13.JPG[]

[source,bash]
----
ip route show
----
---

Validar servidors dns

image::imatges/p3/ex1/14.JPG[]

[source,bash]
----
cat /etc/resolv.conf
----
---

Captura de la instrucció i el resultat

image::imatges/p3/ex1/15.JPG[]

[source,bash]
----
cat /etc/resolv.conf
----
---

Captura de la instrucció i el resultat

image::imatges/p3/ex1/12.JPG[]

[source,bash]
----
en ifconfig -a se pot veure la mac de enp0s3.
----
---

= Apartat 4: Reserva d’adreces IP

Captura de la configuració

image::imatges/p3/ex2/1.JPG[]

[source,bash]
----
Configuracio de dhcp.
----
---

Captura de la instrucció per validar l’adreça IP i del seu resultat.

image::imatges/p3/ex2/2.JPG[]

[source,bash]
----
Validem si la ip es correcta.
----
---

= Apartat 5: Grups d’adreces i clients registrats

Captura de les modificacions de l’arxiu de configuració.

image::imatges/p3/ex2/3.JPG[]

[source,bash]
----
Aqui tenim la configuracio de dhcp amb els pool de ip.
----
---

Captura del terminal amb la renovació i la IP adquirida.

image::imatges/p3/ex2/4.JPG[]

[source,bash]
----
Validem si la ip es correcta segons el pool.
----
---

Captura del terminal amb la renovació i la IP adquirida.

image::imatges/p3/ex2/5.JPG[]

[source,bash]
----
Validem si la ip es correcta segons el pool.
----
---

= Apartat 6: Configuració de PC1 i PC2 a una xarxa diferent

IMPORTANT: Me he dado cuenta que no canvie en nombre al clonar las maquinas pero en el nombre de la barra de virtual box se ve que es la maquina correcta en cada caso. Lo arreglo para el proximo ejercicio.

Captura de la nova configuració.

image::imatges/p3/ex3/1.JPG[]

[source,bash]
----
Captura de la configuracio de Farnsworth de dhcp per la xarxa 172.30.2.0.
----
---

Comprovació de la IP de PC1 i PC2.

image::imatges/p3/ex3/2.JPG[]
image::imatges/p3/ex3/2.2.JPG[]

[source,bash]
----
Aqui podem veure com les ip son correctes en les dos maquines.
----
---

= Apartat 7: Configuració d’un servidor secundari

Instal·lació del NTP a Farnsworth i a Hermes.

image::imatges/p3/ex3/3.JPG[]
image::imatges/p3/ex3/3.1.JPG[]

[source,bash]
----
Instalem el ntp en les dos maquines.
----
---

Canvis a la configuració de Farnsworth.

image::imatges/p3/ex3/4.JPG[]

[source,bash]
----
Configuracio de dhcp de la maquina Farnsworth.
----
---

Demostració que el servidor DHCP s’està executant a Hermes.

image::imatges/p3/ex3/6.JPG[]

[source,bash]
----
Com podem veure esta actiu.
----
---

Captura de la configuració de failover a Farnsworth.

image::imatges/p3/ex3/4.JPG[]

[source,bash]
----
Com podem veure el servidor Farnsworth te la configuracio de failover.
----
---

Captura de la configuració de failover a Hermes.

image::imatges/p3/ex3/5.JPG[]

[source,bash]
----
Com podem veure el servidor Farnsworth te la configuracio de failover.
----
---

Nova configuració del DHCP relay.
Demostració que el failover funciona.

image::imatges/p3/ex3/7.JPG[]

[source,bash]
----
Aqui podem veure com Farnsworth te el servidor dhcp apagat per simular que falla i el
servidor Hermes esta funcionant i es el que dona la ip al client 1 com podem veure. 
Funciona perfectament el failover.
----
---








